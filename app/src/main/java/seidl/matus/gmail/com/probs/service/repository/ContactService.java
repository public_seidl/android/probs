package seidl.matus.gmail.com.probs.service.repository;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import seidl.matus.gmail.com.probs.service.model.ContactsResponse;
import seidl.matus.gmail.com.probs.service.model.NewContact;

/**
 * Created by Matus on 22. 7. 2017.
 */

public interface ContactService {

    @GET("contacts")
    Call<ContactsResponse> getContactsList();

    @POST("contacts")
    Call<Void> createContact(@Body NewContact contact);
}
