package seidl.matus.gmail.com.probs;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import seidl.matus.gmail.com.probs.adapters.DetailContactAdapter;
import seidl.matus.gmail.com.probs.service.ApiClient;
import seidl.matus.gmail.com.probs.service.model.Contact;
import seidl.matus.gmail.com.probs.service.model.Order;
import seidl.matus.gmail.com.probs.service.model.OrdersResponse;
import seidl.matus.gmail.com.probs.viewmodel.DetailContactViewModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailContactsActivityFragment extends LifecycleFragment {

    private DetailContactViewModel viewModel;

    private RecyclerView recyclerView;
    private DetailContactAdapter adapter;

    public DetailContactsActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_contacts, container, false);
        ApiClient.setContext(getContext());
        viewModel = ViewModelProviders.of(getActivity()).get(DetailContactViewModel.class);
        recyclerView = view.findViewById(R.id.detail_contact);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new DetailContactAdapter(new ArrayList<Order>());
        recyclerView.setAdapter(adapter);

        observeViewModel(viewModel, view);
        return view;
    }


    private void observeViewModel(DetailContactViewModel viewModel, final View view) {
        viewModel.getOrderLiveData().observe(this, new Observer<OrdersResponse>() {
            @Override
            public void onChanged(@Nullable OrdersResponse detailCustomer) {
                if (detailCustomer != null) {
                    adapter.setOrders(detailCustomer.getItems());
                }
            }
        });
        viewModel.getActualContact().observe(this, new Observer<Contact>() {
            @Override
            public void onChanged(@Nullable Contact contact) {
                if (contact != null) {
                    getActivity().setTitle(contact.getName());
                    ((TextView) view.findViewById(R.id.id_tel_number)).setText(contact.getPhone());
                }
            }
        });

    }

}
