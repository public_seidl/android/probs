package seidl.matus.gmail.com.probs.service.model;

import java.util.List;

/**
 * Created by Matus on 19. 7. 2017.
 */

public class ContactsResponse {

    private List<Contact> items;

    public ContactsResponse() {
    }

    public ContactsResponse(List<Contact> items) {
        this.items = items;
    }

    public List<Contact> getItems() {
        return items;
    }

    public void setItems(List<Contact> items) {
        this.items = items;
    }
}
