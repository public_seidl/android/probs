package seidl.matus.gmail.com.probs.service.model;

import java.util.List;

/**
 * Created by Matus on 19. 7. 2017.
 */

public class OrdersResponse {

    private List<Order> items;

    public OrdersResponse() {
    }

    public OrdersResponse(List<Order> items) {
        this.items = items;
    }

    public List<Order> getItems() {
        return items;
    }

    public void setItems(List<Order> items) {
        this.items = items;
    }
}
