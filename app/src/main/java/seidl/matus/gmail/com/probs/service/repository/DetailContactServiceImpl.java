package seidl.matus.gmail.com.probs.service.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import seidl.matus.gmail.com.probs.service.ApiClient;
import seidl.matus.gmail.com.probs.service.model.OrdersResponse;

/**
 * Created by Matus on 13. 8. 2017.
 */

public class DetailContactServiceImpl {

    private static DetailContactService service;

    private static DetailContactServiceImpl instance;

    private DetailContactServiceImpl() {

    }

    public static DetailContactServiceImpl getIntace() {
        if (instance == null) {
            service = ApiClient.getClient().create(DetailContactService.class);
            return instance = new DetailContactServiceImpl();
        } else {
            return instance;
        }
    }

    public LiveData<OrdersResponse> getDetailContact(String id) {
        final MutableLiveData<OrdersResponse> data = new MutableLiveData<>();

        service.getDetailContact(id).enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                if (response.code() == 200) {
                    data.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {

            }
        });

        return data;
    }
}
