package seidl.matus.gmail.com.probs;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import seidl.matus.gmail.com.probs.adapters.ContactsAdapter;
import seidl.matus.gmail.com.probs.handler.ContactHandler;
import seidl.matus.gmail.com.probs.service.ApiClient;
import seidl.matus.gmail.com.probs.service.model.Contact;
import seidl.matus.gmail.com.probs.service.model.ContactsResponse;
import seidl.matus.gmail.com.probs.viewmodel.ContactListViewModel;
import seidl.matus.gmail.com.probs.viewmodel.DetailContactViewModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends LifecycleFragment implements ContactHandler {

    private ContactsAdapter contactsAdapter;
    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private ContactListViewModel contactListViewModel;
    private DetailContactViewModel detailContactViewModel;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ApiClient.setContext(getContext());
        contactListViewModel = ViewModelProviders.of(getActivity()).get(ContactListViewModel.class);
        contactListViewModel.loadAllContacts();
        detailContactViewModel = ViewModelProviders.of(getActivity()).get(DetailContactViewModel.class);
        recyclerView = view.findViewById(R.id.contacts_list);

        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);


        contactsAdapter = new ContactsAdapter(new ArrayList<Contact>(), getContext(), this);
        recyclerView.setAdapter(contactsAdapter);

        ///observeViewModel(contactListViewModel);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        observeViewModel(contactListViewModel);
    }

    private void observeViewModel(ContactListViewModel viewModel) {
        viewModel.getContactsResponseLiveData().observe(this, new Observer<ContactsResponse>() {
            @Override
            public void onChanged(@Nullable ContactsResponse projects) {
                if (projects != null) {
                    contactsAdapter.addItems(projects.getItems());
                }
            }
        });
    }

    @Override
    public void onClick(Contact customer) {
        detailContactViewModel.setActualContact(customer);
        detailContactViewModel.findDetailCustomer(customer.getId());
        startActivity(new Intent(getActivity(), DetailContactsActivity.class));

    }
}
