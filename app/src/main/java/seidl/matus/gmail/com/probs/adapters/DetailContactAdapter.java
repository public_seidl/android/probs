package seidl.matus.gmail.com.probs.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import seidl.matus.gmail.com.probs.R;
import seidl.matus.gmail.com.probs.service.model.Order;

/**
 * Created by Matus on 23. 7. 2017.
 */

public class DetailContactAdapter extends RecyclerView.Adapter<DetailContactAdapter.DetailContactHolder> {

    private List<Order> orders;


    public DetailContactAdapter(List<Order> orders) {
        this.orders = orders;

    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
        this.notifyDataSetChanged();
    }


    public void addOrder(Order order) {
        orders.add(order);
        this.notifyDataSetChanged();

    }

    @Override
    public DetailContactAdapter.DetailContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_row, parent, false);
        return new DetailContactHolder(view);
    }


    @Override
    public void onBindViewHolder(DetailContactAdapter.DetailContactHolder holder, final int position) {
        Order order = orders.get(position);
        holder.orderName.setText(order.getName());
        holder.orderValue.setText(order.getCount() + "x");
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }


    static class DetailContactHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView orderName;
        public TextView orderValue;

        public DetailContactHolder(View itemView) {
            super(itemView);
            view = itemView;
            orderName = view.findViewById(R.id.name_of_order_name);
            orderValue = view.findViewById(R.id.name_of_order_value);

        }
    }
}
