package seidl.matus.gmail.com.probs;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import seidl.matus.gmail.com.probs.service.ApiClient;
import seidl.matus.gmail.com.probs.service.model.NewContact;
import seidl.matus.gmail.com.probs.viewmodel.ContactListViewModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class CreateConstactActivityFragment extends LifecycleFragment {

    private ContactListViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_constact, container, false);
        ApiClient.setContext(getContext());
        final EditText name = view.findViewById(R.id.name);
        final EditText phone = view.findViewById(R.id.number);
        Button createBtn = view.findViewById(R.id.create_contact);

        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameTxt = String.valueOf(name.getText());
                String phoneTxt = String.valueOf(phone.getText());

                if (nameTxt.isEmpty()) {
                    name.setError("Name is mandatory");

                }
                if (phoneTxt.isEmpty()) {
                    phone.setError("Phone is mandatory");

                }

                if (0 < nameTxt.length() && nameTxt.length() < 5) {
                    name.setError("Name must have least 5 character");

                }
                if (0 < phoneTxt.length() && phoneTxt.length() < 5) {
                    phone.setError("Phone must have least 5 character");

                }

                final NewContact newContact = new NewContact();
                newContact.setName(nameTxt);
                newContact.setPhone(phoneTxt);

                viewModel = ViewModelProviders.of(getActivity()).get(ContactListViewModel.class);

                viewModel.createContact(newContact);

                viewModel.checkIsCreated().observe(CreateConstactActivityFragment.this, new Observer<Boolean>() {
                    @Override
                    public void onChanged(@Nullable Boolean aBoolean) {
                        if (aBoolean) {
                            viewModel.addNewContact(newContact);
                            getActivity().finish();
                        } else {
                            Toast.makeText(getContext(), "Contact was not created", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        return view;
    }


}
