package seidl.matus.gmail.com.probs.handler;

import seidl.matus.gmail.com.probs.service.model.Contact;

/**
 * Created by Matus on 13. 8. 2017.
 */

public interface ContactHandler {
    void onClick(Contact customer);
}
