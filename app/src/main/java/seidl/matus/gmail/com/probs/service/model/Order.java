package seidl.matus.gmail.com.probs.service.model;

/**
 * Created by Matus on 19. 7. 2017.
 */

public class Order {

    private String name;
    private Long count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
