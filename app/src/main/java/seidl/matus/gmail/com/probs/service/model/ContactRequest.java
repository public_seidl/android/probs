package seidl.matus.gmail.com.probs.service.model;

/**
 * Created by Matus on 19. 7. 2017.
 */

public class ContactRequest {

    private String name;
    private String phone;

    public ContactRequest() {
    }

    public ContactRequest(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
