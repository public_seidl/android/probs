package seidl.matus.gmail.com.probs.service.repository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import seidl.matus.gmail.com.probs.service.model.OrdersResponse;

/**
 * Created by Matus on 13. 8. 2017.
 */

public interface DetailContactService {

    @GET(value = "contacts/{id}/order")
    Call<OrdersResponse> getDetailContact(@Path("id") String id);
}
