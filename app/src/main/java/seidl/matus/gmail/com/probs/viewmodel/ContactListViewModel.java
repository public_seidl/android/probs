package seidl.matus.gmail.com.probs.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import seidl.matus.gmail.com.probs.service.model.Contact;
import seidl.matus.gmail.com.probs.service.model.ContactsResponse;
import seidl.matus.gmail.com.probs.service.model.NewContact;
import seidl.matus.gmail.com.probs.service.repository.ContactServiceImpl;

/**
 * Created by Matus on 22. 7. 2017.
 */

public class ContactListViewModel extends ViewModel {

    private static MutableLiveData<ContactsResponse> contactsResponseLiveData;
    private static MutableLiveData<Boolean> isCreated;


    public void loadAllContacts() {
        if (contactsResponseLiveData == null) {
            contactsResponseLiveData = ContactServiceImpl.getIntace().getContactsList();
        }
    }


    public LiveData<ContactsResponse> getContactsResponseLiveData() {
        return contactsResponseLiveData;
    }

    public void createContact(NewContact contact) {
        isCreated = ContactServiceImpl.getIntace().createContact(contact);
    }

    public LiveData<Boolean> checkIsCreated() {
        return isCreated;
    }

    public void addNewContact(NewContact contact) {
        List<Contact> list = contactsResponseLiveData.getValue().getItems();

        list.add(new Contact(null, contact.getName(), contact.getPhone(), null));
        contactsResponseLiveData.setValue(new ContactsResponse(list));
    }


}
