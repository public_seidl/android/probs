package seidl.matus.gmail.com.probs.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import seidl.matus.gmail.com.probs.R;
import seidl.matus.gmail.com.probs.handler.ContactHandler;
import seidl.matus.gmail.com.probs.service.model.Contact;

/**
 * Created by Matus on 23. 7. 2017.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactHolder> {

    private List<Contact> contactList;
    private Context context;
    private ContactHandler contactHandler;


    public ContactsAdapter(List<Contact> contactList, Context context, ContactHandler contactHandler) {
        this.contactList = contactList;
        this.context = context;
        this.contactHandler = contactHandler;
    }

    @Override
    public ContactsAdapter.ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_row, parent, false);
        return new ContactHolder(view);
    }


    @Override
    public void onBindViewHolder(ContactsAdapter.ContactHolder holder, final int position) {
        Contact contact = contactList.get(position);
        holder.name.setText(contact.getName());
        holder.phone.setText(contact.getPhone());
        Picasso.with(context)
                .load(contact.getPictureUrl())
                .error(R.drawable.image_not_found)
                .placeholder(R.drawable.image_not_found)
                .into(holder.profilPhoto);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactHandler.onClick(contactList.get(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public void addItems(List<Contact> contacts) {
        this.contactList = contacts;
        notifyDataSetChanged();
    }

    public void addItem(Contact contact) {
        this.contactList.add(contact)
        ;
        notifyDataSetChanged();


    }


    static class ContactHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView phone;
        public TextView name;
        public ImageView profilPhoto;

        public ContactHolder(View itemView) {
            super(itemView);
            view = itemView;
            phone = (TextView) view.findViewById(R.id.ic_contact_phone);
            name = (TextView) view.findViewById(R.id.id_contact_name);
            profilPhoto = (ImageView) view.findViewById(R.id.id_contact_photo);

        }
    }
}
