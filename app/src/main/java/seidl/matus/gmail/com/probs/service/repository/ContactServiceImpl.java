package seidl.matus.gmail.com.probs.service.repository;

import android.arch.lifecycle.MutableLiveData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import seidl.matus.gmail.com.probs.service.ApiClient;
import seidl.matus.gmail.com.probs.service.model.ContactsResponse;
import seidl.matus.gmail.com.probs.service.model.NewContact;

/**
 * Created by Matus on 22. 7. 2017.
 */

public class ContactServiceImpl {

    private static ContactService service;

    private static ContactServiceImpl instance;

    private ContactServiceImpl() {

    }

    public static ContactServiceImpl getIntace() {
        if (instance == null) {
            service = ApiClient.getClient().create(ContactService.class);
            return instance = new ContactServiceImpl();
        } else {
            return instance;
        }
    }

    public MutableLiveData<ContactsResponse> getContactsList() {
        final MutableLiveData<ContactsResponse> data = new MutableLiveData<>();

        service.getContactsList().enqueue(new Callback<ContactsResponse>() {
            @Override
            public void onResponse(Call<ContactsResponse> call, Response<ContactsResponse> response) {
                if (response.code() == 200) {
                    data.setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<ContactsResponse> call, Throwable t) {

            }
        });

        return data;
    }

    public MutableLiveData<Boolean> createContact(NewContact contact) {
        final MutableLiveData<Boolean> isCreated = new MutableLiveData<>();
        service.createContact(contact).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    isCreated.setValue(true);
                } else {
                    isCreated.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                isCreated.setValue(false);
            }
        });
        return isCreated;
    }
}
