package seidl.matus.gmail.com.probs.service.model;

/**
 * Created by Matus on 19. 7. 2017.
 */

public class Contact {
    private String id;
    private String name;
    private String phone;
    private String pictureUrl;


    public Contact(String id, String name, String phone, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.pictureUrl = pictureUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
