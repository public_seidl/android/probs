package seidl.matus.gmail.com.probs.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import seidl.matus.gmail.com.probs.service.model.Contact;
import seidl.matus.gmail.com.probs.service.model.OrdersResponse;
import seidl.matus.gmail.com.probs.service.repository.DetailContactServiceImpl;

/**
 * Created by Matus on 13. 8. 2017.
 */

public class DetailContactViewModel extends ViewModel {

    private static LiveData<OrdersResponse> orderLiveData;

    private static MutableLiveData<Contact> selected = new MutableLiveData<Contact>();


    public void findDetailCustomer(String id) {
        orderLiveData = DetailContactServiceImpl.getIntace().getDetailContact(id);
    }

    public LiveData<OrdersResponse> getOrderLiveData() {
        return orderLiveData;
    }

    public LiveData<Contact> getActualContact() {
        return selected;
    }

    public void setActualContact(Contact actualContact) {
        selected.setValue(actualContact);

    }
}

